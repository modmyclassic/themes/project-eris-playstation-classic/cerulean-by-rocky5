#!/bin/sh
source "/var/volatile/project_eris.cfg"
source "${THEMES_PATH}/${SELECTED_THEME}/launchers/sdl_settings.cfg"
GAME_SOURCE="/gaadata"
USB_SOURCE="${MOUNTPOINT}/internal games backup"
rm -f /tmp/sdldisplaycmd
rm -f "${USB_SOURCE}/Information.txt"
sdl_text "This can take some time"
sleep 3
start=$(date +%s)
echo -e "Folder Name\t\t-\t\tGame Name\n" >> "${USB_SOURCE}/Information.txt"
for i in {1..20}
	do
		if [ ! -f "${USB_SOURCE}/databases/regional.db" ]; then mkdir -p "${USB_SOURCE}/databases/"; cp "${GAME_SOURCE}/databases/regional.db" "${USB_SOURCE}/databases/"; fi
		if [ ! -d "${USB_SOURCE}/$i" ]; then mkdir -p "${USB_SOURCE}/$i"; fi
		lic_file="$(find ${GAME_SOURCE}/$i/ -name '*.lic')"
		game_name=$(head -n 1 $lic_file | sed -e "s/[^ A-Za-z0-9]//g")
		echo -e "\t$i\t\t\t-\t$game_name" >> "${USB_SOURCE}/Information.txt"
		for x in "${GAME_SOURCE}/$i"/*
		do
			echo 1 > /sys/class/leds/green/brightness & echo 0 > /sys/class/leds/red/brightness
			source_size=`stat -c%s $x`
			usb_size=`stat -c%s "${USB_SOURCE}/$i/${x##*/}"`
			if [ "$source_size" = "$usb_size" ]; then
				sdl_text  "Checking $game_name"
			else
				sdl_text  "Backing up $game_name"
				echo 1 > /sys/class/leds/green/brightness & echo 1 > /sys/class/leds/red/brightness
				cp "$x" "${USB_SOURCE}/$i/"
				backup_started=1
			fi
		done
done
end=$(date +%s)
if [ $backup_started = "1" ]; then
	if [ $(((end-start)/60)) = "0" ]; then
		sdl_text "Complete: Backup took $(((end-start)%60)) Seconds"
	else
		sdl_text "Complete: Backup took $(((end-start)/60)) Minutes & $(((end-start)%60)) Seconds"
	fi
else
	sdl_text "Complete: No backup required"
fi
echo -e "\nBackup took $(((end-start)/60)) Minutes & $(((end-start)%60)) Seconds" >> "${USB_SOURCE}/Information.txt"
sleep 5

