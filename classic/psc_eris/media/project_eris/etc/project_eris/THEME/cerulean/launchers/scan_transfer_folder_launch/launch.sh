#!/bin/sh
source "/var/volatile/project_eris.cfg"
source "${THEMES_PATH}/${SELECTED_THEME}/launchers/sdl_settings.cfg"
rm -f /tmp/sdldisplaycmd
chmod +x "${PROJECT_ERIS_PATH}/opt/psc_transfer_tools/psc_game_add"
cd "${PROJECT_ERIS_PATH}/opt/psc_transfer_tools"
sdl_text "Scanning transfer directory for games"
sleep 3
"${PROJECT_ERIS_PATH}/opt/psc_transfer_tools/psc_game_add" "${MOUNTPOINT}/transfer/" "${PROJECT_ERIS_PATH}/etc/project_eris/SYS/databases/" "${MOUNTPOINT}/games/"
sdl_text "Complete"
sleep 3